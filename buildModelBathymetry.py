from demBuilder import SamplePoints
from demBuilder import SamplePointReader
from demBuilder import SamplePointFormat
from demBuilder import BoundaryPolygonType
from demBuilder import ResampleMethods
from demBuilder import Raster
import numpy.ma as ma
import numpy as np
import pylab as pl
import os
from descartes import PolygonPatch
import gdal, ogr, os, osr
import shapefile
from matplotlib import path


if __name__ == '__main__':


    bbox=np.zeros(4)
    bbox[0] = 20000
    bbox[1] = 7420000
    bbox[2] = 524000
    bbox[3] = 7888000


    print('Creating raster')
    raster = Raster(bbox=bbox, resolution=100, epsgCode=28350)

    buffer = 20000
    sampleBbox = np.zeros(4)
    sampleBbox[0] = bbox[0] - buffer
    sampleBbox[1] = bbox[1] - buffer
    sampleBbox[2] = bbox[2] + buffer
    sampleBbox[3] = bbox[3] + buffer

    folderPath = "ChannelCoast"
    for fn in os.listdir(folderPath):
        if os.path.isfile(folderPath + fn):
            print("Processing " + fn)
            surveyDataReader = SamplePointReader(folderPath + fn, cropTo=sampleBbox)
            surveyData = surveyDataReader.load()

            if len(surveyData.z) > 0:
                surveyData.resample(raster, ResampleMethods.BlockAvg)
            surveyData = None

    folderPath = ""
    for fn in os.listdir(folderPath):
        if os.path.isfile(folderPath + fn):
            print("Processing " + fn)
            ladsDataReader = SamplePointReader(folderPath + fn, cropTo=sampleBbox)
            ladsData = ladsDataReader.load()

            if len(ladsData.z) > 0:
                ladsData.resample(raster, ResampleMethods.BlockAvg)
            ladsData = None

    print("Processing SRTM")

    # put in SRTM saqmple for land areas as better than GA
    folderPath = "bathy/SRTM/"
    srtmDownSampled = Raster(bbox=sampleBbox, resolution=200, epsgCode=28350)
    for fn in os.listdir(folderPath):
        if os.path.isfile(folderPath + fn):
            print("Processing " + fn)
            srtmDataReader = SamplePointReader(folderPath + fn, coordConvert=True,
                                               cropTo=sampleBbox)
            srtmData = srtmDataReader.load()

            if fn == "s22_e115_1arc_v3.tif":
                filterAreaFile = shapefile.Reader("shapeFiles/SRTM_OnslowJetty.shp")
                filterArea = filterAreaFile.shape(0)
                boundaryPath = path.Path(filterArea.points)
                inds = boundaryPath.contains_points(np.stack((srtmData.x, srtmData.y), axis=0).transpose())
                srtmData.remove(inds)

            if len(srtmData.z) > 0:
                srtmData.resample(raster, ResampleMethods.BlockAvg)
                srtmData.resample(srtmDownSampled, ResampleMethods.BlockAvg)
            srtmData = None


    print("Processing other Bathy")

    folderPath = ""
    for fn in os.listdir(folderPath):
        if os.path.isfile(folderPath + fn):
            combinedRasterReader = SamplePointReader(folderPath + fn, cropTo=sampleBbox)
            combinedRaster = combinedRasterReader.load()
            combinedRaster.z = combinedRaster.z - 1.6
            combinedRaster.resample(raster, ResampleMethods.BlockAvg)


    print("Processing GA 250m Data")

    filterAreaFile = shapefile.Reader("shapeFiles/GA250mExtension.shp")
    filterArea = filterAreaFile.shape(0)

    srtmSamples = srtmDownSampled.getSamples()


    gaDataReader = SamplePointReader("GA250m_Clipped.tif",
                                     coordConvert=True, cropTo=sampleBbox)
    gaData = gaDataReader.load()
    gaData.generateBoundary(BoundaryPolygonType.Polygon, points=filterArea.points)
    boundaryPath = path.Path(filterArea.points)
    inds = boundaryPath.contains_points(np.stack((gaData.x, gaData.y), axis=0).transpose())
    gaData.remove(~inds)
    gaData.z = gaData.z - 2.4
    gaData.resample(raster, ResampleMethods.Linear)




    print("Processing AHO Data")
    # ahoDataReader = SamplePointReader("C:/Work/Data/Corrected_Pilbara_Bathy/AHO_S57_3d_points-_RevUWA0.txt")
    # ahoData = ahoDataReader.load()
    ahoDataReader = SamplePointReader("AHO_S57_3d_points.csv",
                                      coordConvert=True, cropTo=sampleBbox)
    ahoData = ahoDataReader.load(headerLines=1, delimiter=",", delim_whitespace=False)
    ahoData.z = (ahoData.z * -1) - 1.489  # Chart is typically in mLAT which is -1.489 mAHD at Onslow

    filterAreaFile = shapefile.Reader("shapeFiles/AHOBadPoints.shp")
    filterArea = filterAreaFile.shape(0)
    boundaryPath = path.Path(filterArea.points)
    inds = boundaryPath.contains_points(np.stack((ahoData.x, ahoData.y), axis=0).transpose())
    ahoData.remove(inds)

    contoursDataReader = SamplePointReader(
        "nearshoreContours_100mIntvl_MGAz50_mLAT.csv", coordConvert=False,
        cropTo=sampleBbox)
    contourData = contoursDataReader.load(headerLines=1, delimiter=",", delim_whitespace=False)
    contourData.z = (contourData.z * -1) - 1.489  # Chart is typically in mLAT which is -1.489 mAHD at Onslow

    ahoData.appendSamples(contourData.x, contourData.y, contourData.z)

    ahoData.generateBoundary(BoundaryPolygonType.Box)
    ahoData.resample(raster,ResampleMethods.Linear)

    raster.smooth(1.5)

    raster.saveToFile('FinalBathy_Linear.tif')

    fig = pl.figure(figsize=(15, 10))
    ax = fig.add_subplot(111)
    margin = 250
    x_min, y_min, x_max, y_max = bbox
    ax.set_xlim([x_min - margin, x_max + margin])
    ax.set_ylim([y_min - margin, y_max + margin])

    pl.pcolormesh(newRaster.xBinEdges, newRaster.yBinEdges, newRaster.z, vmin=-50, vmax=0)
    # pl.scatter(ahoData.x, ahoData.y, 70, ahoData.z, vmin=-50, vmax=0)

    pl.show()
